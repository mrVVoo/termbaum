package de.tu_dresden.inf.srz.inf3;

/**
 * Created by Markus Wutzler on 23/06/15.
 */
public class DivisionsOperator extends Operator {
    @Override
    public String getOperator() {
        return "/";
    }

    @Override
    public double berechne() {
        // durch 0??? kann hier behandelt werden!
        return this.getLinkerTerm().berechne()/this.getRechterTerm().berechne();
    }
}
