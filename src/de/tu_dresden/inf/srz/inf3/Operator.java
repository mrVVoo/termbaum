package de.tu_dresden.inf.srz.inf3;

/**
 * Created by Markus Wutzler on 23/06/15.
 */
public abstract class Operator extends Term {
    private Term links;
    private Term rechts;

    public Term getLinkerTerm() {
        return links;
    }

    public void setLinkerTerm(Term links) {
        this.links = links;
    }

    public Term getRechterTerm() {
        return rechts;
    }

    public void setRechterTerm(Term rechts) {
        this.rechts = rechts;
    }

    public abstract String getOperator();

    @Override
    public String getTerm() {
        return "("+links.getTerm()+getOperator()+rechts.getTerm()+")";
    }
}
