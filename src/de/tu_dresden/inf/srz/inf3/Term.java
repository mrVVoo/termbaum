package de.tu_dresden.inf.srz.inf3;

/**
 * Created by Markus Wutzler on 23/06/15.
 */
public abstract class Term {
    public abstract String getTerm();
    public abstract double berechne();
}
