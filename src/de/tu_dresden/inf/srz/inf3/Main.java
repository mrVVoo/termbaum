package de.tu_dresden.inf.srz.inf3;

public class Main {

    public static void main(String[] args) {
	// 1 / 4 + 2 * 3
        Operand o1,o2,o3,o4;
        o1 = new Operand(1);
        o2 = new Operand(2);
        o3 = new Operand(3);
        o4 = new Operand(4);

        PlusOperator po = new PlusOperator();
        MultiplikationsOperator mo = new MultiplikationsOperator();
        DivisionsOperator divo = new DivisionsOperator();

        po.setLinkerTerm(divo);
        po.setRechterTerm(mo);

        divo.setLinkerTerm(o1);
        divo.setRechterTerm(o4);

        mo.setLinkerTerm(o2);
        mo.setRechterTerm(o3);

        Term t = po;

        System.out.println(t.getTerm());
        System.out.println(t.berechne());
    }
}
