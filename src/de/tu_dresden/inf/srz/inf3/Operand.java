package de.tu_dresden.inf.srz.inf3;

/**
 * Created by Markus Wutzler on 23/06/15.
 */
public class Operand extends Term {

    private int operand;

    public Operand(int operand) {
        this.operand = operand;
    }

    public int getOperand() {
        return operand;
    }

    public void setOperand(int operand) {
        this.operand = operand;
    }

    @Override
    public String getTerm() {
        return String.valueOf(getOperand());
    }

    @Override
    public double berechne() {
        return (double)operand;
    }
}
